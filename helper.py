import matplotlib.pyplot as plt
from IPython import display

plt.ion()

def plot(scores, mean_scores):
    display.clear_output(wait=True)
    display.display(plt.gcf())
    plt.clf()
    plt.title('Training...')
    plt.xlabel('No Of Games')
    plt.ylabel('Score')
    plt.ylim(0,100)
    plt.xlim(0,1000)
    plt.plot(scores)
    plt.plot(mean_scores)
    plt.show()
    plt.pause(1)
    # plt.text(len(scores)-1, scores[-1], str(scores[-1]))
    # plt.text(len(mean_scores)-1, mean_scores[-1], str(mean_scores[-1]))