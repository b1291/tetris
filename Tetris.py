import pygame
import random
import numpy as npy

colors = [
    (0, 0, 0),
    (120, 37, 179),
    (100, 179, 179),
    (80, 34, 22),
    (80, 134, 22),
    (180, 34, 22),
    (180, 34, 122),
    (20, 108, 133)
]


class Figure:
    x = 0
    y = 0

    figures = [
        [[1, 5, 9, 13], [4, 5, 6, 7]],
        [[4, 5, 9, 10], [2, 6, 5, 9]],
        [[6, 7, 9, 10], [1, 5, 6, 10]],
        [[1, 2, 5, 9], [0, 4, 5, 6], [1, 5, 9, 8], [4, 5, 6, 10]],
        [[1, 2, 6, 10], [5, 6, 7, 9], [2, 6, 10, 11], [3, 5, 6, 7]],
        [[1, 4, 5, 6], [1, 4, 5, 9], [4, 5, 6, 9], [1, 5, 6, 9]],
        [[1, 2, 5, 6]],
    ]

    def __init__(self, x, y, type):
        self.x = x
        self.y = y
        self.type = type
        self.color = type + 1
        # print(self.color)
        self.rotation = 0

    def image(self):
        return self.figures[self.type][self.rotation]

    def rotate(self):
        self.rotation = (self.rotation + 1) % len(self.figures[self.type])


class Tetris:
    level = 2
    score = 0
    state = "start"
    field = []
    height = 0
    width = 0
    x = 100
    y = 60
    zoom = 20
    figure = None
    queue = []

    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.field = []
        self.score = 0
        self.state = "start"
        self.queue = []
        self.fill_tetromino_queue()
        for i in range(height):
            new_line = []
            for j in range(width):
                new_line.append(0)
            self.field.append(new_line)

    def new_figure(self):
        #print(self.field)  # this will be the state
        # print(self.queue)
        if len(self.queue) < 8:
            self.fill_tetromino_queue()
        self.figure = Figure(3, 0, self.queue[0])
        self.queue.pop(0)

    def fill_tetromino_queue(self):
        partial_queue = []
        partial_queue = list(range(0, 7))
        random.shuffle(partial_queue)
        self.queue.extend(partial_queue)

    def intersects(self):
        intersection = False
        for i in range(4):
            for j in range(4):
                if i * 4 + j in self.figure.image():
                    if i + self.figure.y > self.height - 1 or \
                            j + self.figure.x > self.width - 1 or \
                            j + self.figure.x < 0 or \
                            self.field[i + self.figure.y][j + self.figure.x] > 0:
                        intersection = True
        return intersection

    def break_lines(self):
        lines = 0
        lines_broken = 0
        for i in range(1, self.height):
            zeros = 0
            for j in range(self.width):
                if self.field[i][j] == 0:
                    zeros += 1
            if zeros == 0:
                lines += 1
                lines_broken += 1
                for i1 in range(i, 1, -1):
                    for j in range(self.width):
                        self.field[i1][j] = self.field[i1 - 1][j]
        self.score += lines ** 2
        return lines_broken*100

    def go_space(self):
        while not self.intersects():
            self.figure.y += 1
        self.figure.y -= 1
        reward = self.freeze()
        penalty = self.getLevelPenalty()
        reward = reward-penalty
        return reward

    def go_down(self):
        reward = 0
        self.figure.y += 1
        if self.intersects():
            self.figure.y -= 1
            reward = self.freeze()
            penalty = self.getLevelPenalty()
            reward = reward-penalty

        return reward

    def freeze(self):
        for i in range(4):
            for j in range(4):
                if i * 4 + j in self.figure.image():
                    self.field[i + self.figure.y][j +
                                                  self.figure.x] = self.figure.color
        br_lines = self.break_lines()
        self.new_figure()
        if self.intersects():
            self.state = "gameover"
        return br_lines

    def go_side(self, dx):
        old_x = self.figure.x
        self.figure.x += dx
        if self.intersects():
            self.figure.x = old_x

    def rotate(self):
        old_rotation = self.figure.rotation
        self.figure.rotate()
        if self.intersects():
            self.figure.rotation = old_rotation

    def getState(self):
        list = []
        for ar in self.field:
            for no in ar:
                list.append(no)
        return list

    def getLevelPenalty(self):
        column_values = [0,0,0,0,0,0,0,0,0,0]
        penalty = 0
        for row in self.field:
            for cell in row:
                if cell==1:
                    column_values[cell]+=1
        sum = 0
        for value in column_values:
            sum+=value
        avg = int(sum / 10)
        for val in column_values:
            if (abs(avg - val)>1):
                penalty+=penalty
            if (abs(avg - val)>3):
                penalty=penalty+2
        return penalty


class Game:
    # Initialize the game engine
    pygame.init()

    # Define some colors
    BLACK = (0, 0, 0)
    WHITE = (255, 255, 255)
    GRAY = (128, 128, 128)

    size = (400, 500)
    screen = pygame.display.set_mode(size)

    pygame.display.set_caption("Tetris")

    # Loop until the user clicks the close button.
    done = False
    clock = pygame.time.Clock()
    fps = 25
    game = Tetris(20, 10)
    counter = 0

    pressing_down = False

    # while not done:

    # def start(self):
    #     while not self.done:
    #         game_over, score, reward = self.game_step()
    #         if(game_over):
    #             self.reset()
    #     pygame.quit()

    def reset(self):
        self.BLACK = (0, 0, 0)
        self.WHITE = (255, 255, 255)
        self.GRAY = (128, 128, 128)

        self.size = (400, 500)
        self.screen = pygame.display.set_mode(self.size)

        pygame.display.set_caption("Tetris")

        # Loop until the user clicks the close button.
        self.done = False
        self.clock = pygame.time.Clock()
        self.fps = 100
        self.game = Tetris(20, 10)
        self.counter = 0

        self.pressing_down = False
        self.game.__init__(20, 10)

    def getState(self):
        #print(self.game.getState())
        return self.game.getState()

    def game_step(self, action):
        reward = 0

        if self.game.figure is None:
            self.game.new_figure()
        self.counter += 1
        if self.counter > 100000:
            self.counter = 0

        if self.counter % (self.fps // self.game.level // 2) == 0 or self.pressing_down:
            if self.game.state == "start":
                reward += self.game.go_down()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.done = True
            # if event.type == pygame.KEYDOWN:
            #     if event.key == pygame.K_UP:
            #         self.game.rotate()
            #     if event.key == pygame.K_DOWN:
            #         self.pressing_down = True
            #     if event.key == pygame.K_LEFT:
            #         self.game.go_side(-1)
            #     if event.key == pygame.K_RIGHT:
            #         self.game.go_side(1)
            #     if event.key == pygame.K_SPACE:
            #         self.game.go_space()
            #     if event.key == pygame.K_ESCAPE:
            #         self.game.__init__(20, 10)

            # if event.type == pygame.KEYUP:
            #     if event.key == pygame.K_DOWN:
            #         self.pressing_down = False
        if (npy.array_equal(action, [1, 0, 0, 0, 0, 0])):
            self.game.rotate()
        if (npy.array_equal(action, [0, 1, 0, 0, 0, 0])):
            self.pressing_down = not self.pressing_down
        if (npy.array_equal(action, [0, 0, 1, 0, 0, 0])):
            self.game.go_side(-1)
        if (npy.array_equal(action, [0, 0, 0, 1, 0, 0])):
            self.game.go_side(1)
        if (npy.array_equal(action, [0, 0, 0, 0, 1, 0])):
            reward += self.game.go_space()

        self.screen.fill(self.WHITE)
        for i in range(self.game.height):
            for j in range(self.game.width):
                pygame.draw.rect(self.screen, self.GRAY, [
                    self.game.x + self.game.zoom * j, self.game.y + self.game.zoom * i, self.game.zoom, self.game.zoom], 1)
                if self.game.field[i][j] > 0:
                    pygame.draw.rect(self.screen, colors[self.game.field[i][j]],
                                     [self.game.x + self.game.zoom * j + 1, self.game.y + self.game.zoom * i + 1, self.game.zoom - 2, self.game.zoom - 1])

        if self.game.figure is not None:
            for i in range(4):
                for j in range(4):
                    p = i * 4 + j
                    if p in self.game.figure.image():
                        pygame.draw.rect(self.screen, colors[self.game.figure.color],
                                         [self.game.x + self.game.zoom * (j + self.game.figure.x) + 1,
                                          self.game.y + self.game.zoom *
                                          (i + self.game.figure.y) + 1,
                                          self.game.zoom - 2, self.game.zoom - 2])

        font = pygame.font.SysFont('Calibri', 25, True, False)
        font1 = pygame.font.SysFont('Calibri', 65, True, False)
        text = font.render("Score: " + str(self.game.score), True, self.BLACK)
        text_game_over = font1.render("Game Over", True, (255, 125, 0))
        text_game_over1 = font1.render("Press ESC", True, (255, 215, 0))

        self.screen.blit(text, [0, 0])
        game_over = False
        if self.game.state == "gameover":
            # screen.blit(text_game_over, [20, 200])
            # screen.blit(text_game_over1, [25, 265])
            reward = -20
            ac_score = self.game.score
            game_over = True
            #self.reset()
            return reward, True, ac_score
        else:
            if reward==0:
                reward = 1

        pygame.display.flip()
        self.clock.tick(self.fps)
        return reward, game_over, self.game.score


# g = Game()
# g.start()
