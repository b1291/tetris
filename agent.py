import torch
import random
import numpy as np
from collections import deque
from Tetris import Game, Tetris
from model import Linear_QNet, QTrainer
from helper import plot

MAX_MEMORY = 100_000
BATCH_SIZE = 1000
LR = 0.001


class Agent:
    def __init__(self):
        self.no_games = 0
        self.epsilon = 0
        self.gamma = 0.9
        self.memory = deque(maxlen=MAX_MEMORY)
        self.model = Linear_QNet(200,256,5)
        self.trainer = QTrainer(self.model, lr=LR, gamma=self.gamma)
        #model, trainer

    def get_state(self, game):
        return np.array(game.getState(), dtype=int)

    def remember(self, state, action, reward, next_state, game_over):
        # popleft if max_mem reached
        self.memory.append((state, action, reward, next_state, game_over))

    def train_long_memory(self):
        if len(self.memory) > BATCH_SIZE:
            mini_sample = random.sample(
                self.memory, BATCH_SIZE)  # --> list of tuples
        else:
            mini_sample = self.memory

        states, actions, rewards, next_states, game_overs = zip(*mini_sample)

        self.trainer.train_step(states, actions, rewards,
                                next_states, game_overs)

    def train_short_memory(self, state, action, reward, next_state, game_over):
        self.trainer.train_step(state, action, reward, next_state, game_over)

    def get_action(self, state):
        # random moves : tradeoff exploration/exploitation
        self.epsilon = 80 - self.no_games
        final_move = [0, 0, 0, 0, 0, 0]
        if random.randint(0, 200) < self.epsilon:
            move = random.randint(0, 4)
            final_move[move] = 1
        else:
            state0 = torch.tensor(state, dtype=torch.float)
            prediction = self.model(state0)
            move = torch.argmax(prediction).item()
            final_move[move] = 1
        return final_move


def train():
    plot_scores = []
    plot_avg_scores = []
    total_score = 0
    high_score = 0
    agent = Agent()
    game = Game()
    print(game)
    while True:
        # get old state
        state_old = agent.get_state(game)

        # get move
        final_move = agent.get_action(state_old)

        # perform move and get new state
        reward, done, score = game.game_step(final_move)
        state_new = agent.get_state(game)

        # train short memory
        agent.train_short_memory(
            state_old, final_move, reward, state_new, done)

        # remember
        agent.remember(state_old, final_move, reward, state_new, done)

        if done:
            # train long memory, plot result
            game.reset()
            agent.no_games += 1
            agent.train_long_memory()
            if score > high_score:
                high_score = score
                agent.model.save()
            print('Game ', agent.no_games, 'Score ',
                  score, 'HighScore ', high_score)

            plot_scores.append(score)
            print('plot_scores ', plot_scores)
            total_score+=score
            mean_score = total_score / agent.no_games
            plot_avg_scores.append(mean_score)
            print('avg scores', plot_avg_scores)
            plot(plot_scores, plot_avg_scores)


if __name__ == '__main__':
    train()
